package com.devsuperior.controllers;

import com.devsuperior.entities.User;
import com.devsuperior.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/users")
public class UserController {

    @Autowired
    private UserRepository ur;

    @GetMapping
    public List<User> listarUsers(){
        List<User> result = ur.findAll();
        return result;
    }

    @GetMapping(value = "/{id}")
    public User listUser(@PathVariable long id){
        User result = ur.findById(id).get();
        return result;
    }

    @PostMapping
    public User insert(@RequestBody User user){
        User result = ur.save(user);
        return result;
    }

}
